import Heroe from './Heroe_clase.js';

export default class Druida extends Heroe{
    constructor(nombre, experiencia, puntos_vida, puntos_ataque, defensa, imagen, pociones, longitud_barba){
        super(nombre, experiencia, puntos_vida, puntos_ataque,defensa,imagen);
        this._pociones = pociones;
        this._longitud_barba = longitud_barba;
    }

    ataque(enemigo){
        if (this._longitud_barba >= 30)
        {
            var dolor = Math.floor(Math.random()*this._puntos_ataque) +3;
            enemigo._puntos_vida -= dolor;
            return "Has lanzado un hechizo bastante poderoso";
        }
        else
        {
            var dolor = Math.floor(Math.random()*this._puntos_ataque)/3;
            enemigo._puntos_vida -= dolor;

            return "Continúa con tu retiro espiritual y vuelve con una barba en condiciones"
        }
    }
}

