import Heroe from './Heroe_clase.js';

export default class Guerrero extends Heroe{
    constructor(nombre, experiencia, puntos_vida, puntos_ataque, defensa, imagen, armas, resistencia_armadura, montura){
        super(nombre, experiencia, puntos_vida, puntos_ataque, defensa, imagen);
        this.armas = armas;
        this._resistencia_armadura = resistencia_armadura;
        this._montura = montura;
    }

    ataque(enemigo){
        if (this._montura == true)
        {
            var dolor = Math.floor(Math.random()*this._puntos_ataque) +1;
            enemigo._puntos_vida -= dolor;
            //console.log(dolor);
            return "Ataque desde el caballo, que tiemblen";
        }
        else {
            var dolor = Math.floor(Math.random()*this._puntos_ataque)/2;
            enemigo._puntos_vida -= dolor;
            return "Aprovecha tu agilidad y ataca primero o huye mientras estés a tiempo";
        }
    }
}
