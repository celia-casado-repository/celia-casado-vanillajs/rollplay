export default class Heroe {
    constructor (nombre, experiencia, puntos_vida, puntos_ataque, defensa, imagen){
        this._nombre = nombre;
        this._experiencia = experiencia;
        this._puntos_vida = puntos_vida;
        this._puntos_ataque = puntos_ataque;
        this._defensa = defensa;
        this.imagen = imagen;
    }
    ataque(enemigo){

        if (this._puntos_vida >= 20){
            enemigo._puntos_vida -= Math.floor(Math.random()*30);
            //console.log(this);
            return "Sí, vas a tener migrañas con ese cabezazo, pero tu contrincante va a tardar en recordar como se llama"
        }
        else{
            return "Todo son estrellitas de colores, ¿que ha pasado?, intenta correr"
        }
    }
}
