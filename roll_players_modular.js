import Heroe from './Heroe_clase.js';
import Guerrero from './Guerrero.js';
import Druida from './Druida.js';
import Elfo from './Elfo.js';

var carlos = new Heroe ("Carlos", 0, 100, 70, 30, "./assets/heroe.png");
var edu = new Guerrero ("Edu", 0, 100, 50, 30, "./assets/guerrero.png", "espada", 20, true);
var rober = new Druida ("Rober", 0, 100, 30, 50, "./assets/druida.png", "false", 29);
var alex = new Elfo ("Alex", 0, 100, 60, 50, "./assets/elfo.png", "true", 30);


var heroes = [];
heroes.push(carlos);
heroes.push(rober);
heroes.push(edu);
heroes.push(alex);

var template = document.querySelector("#tarjetas");
var tarjeta = template.content.querySelector("div");


for(var i=0; i < heroes.length; i++){
    var clonTemplate = tarjeta.cloneNode(true);
    clonTemplate.id="personajes-heroes" + i;

    clonTemplate.querySelector("img").src = heroes[i].imagen;
    clonTemplate.querySelector("h5").innerHTML= heroes[i]._nombre;

    var btnAtacar = clonTemplate.querySelector(".boton-ataque");
    btnAtacar.addEventListener("click", ataque);


    document.querySelector("#items").appendChild(clonTemplate);

    var elegirEnemigo = clonTemplate.querySelector("#seleccion");
    for(var j=0; j< heroes.length; j++){

        if(heroes[j]._nombre != clonTemplate.querySelector("h5").innerHTML){

            var opciones = document.createElement("option");
            opciones.value = j;
            opciones.innerHTML = heroes[j]._nombre;
            elegirEnemigo.appendChild(opciones);
        }
    }
}

function ataque(){
    var elQueAtaca = this.parentNode.querySelector("h5").textContent;
    var elAtacado = this.parentNode.querySelector("#seleccion").value;
    var divPersonaje = document.querySelector("#personajes-heroes" + elAtacado);
    var heroeAtacado = heroes[elAtacado];

    var encontrarAtacante = heroes.find((Heroe)=>
    {return Heroe._nombre == elQueAtaca;});

    var encontrarAtacado = heroes.find((Heroe)=>
    {return Heroe._nombre == heroeAtacado._nombre;});

    encontrarAtacante.ataque(encontrarAtacado);

    divPersonaje.querySelector("progress").value = heroeAtacado._puntos_vida;

    var invisible = document.querySelector("#personajes-heroes" + elAtacado);

    if (heroeAtacado._puntos_vida <= 0) {
        alert("Esta muerto, y con la comida no se juega");
        invisible.classList.add("invisible");
    }

}
