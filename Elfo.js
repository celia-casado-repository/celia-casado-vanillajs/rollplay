import Heroe from './Heroe_clase.js';

export default class Elfo extends Heroe{
    constructor(nombre, experiencia, puntos_vida, puntos_ataque, defensa, imagen, espada, destreza){
        super(nombre, experiencia, puntos_vida, puntos_ataque, defensa, imagen);
        this._espada = espada;
        this._destreza = destreza;
    }
    ataque(enemigo){
        if (this._espada == true){
            var dolor = Math.floor(Math.random()*this._puntos_ataque)+1;
            enemigo._puntos_vida -= dolor;
            //console.log(dolor);
            return "Parece que además de brillar, tu espada pincha, úsala"
        }
        else {
            var dolor = Math.floor(Math.random()*this._puntos_ataque)/2;
            enemigo._puntos_vida -= dolor;
            return "Busca un palo o algo"
        }
    }
}
