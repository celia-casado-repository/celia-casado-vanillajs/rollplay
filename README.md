Al descargar la carpeta es necesario:

1. Abrirla en consola y ejecutar http-server
2. Acudir al puerto en el que se indica que está escuchando
3. Abrir el archivo roll_players.html

Para la instalación del http-server, en caso de no disponer de este, desde npm es necesario ejecutar el comando  npm install http-server -g
Información adicional sobre el uso de http-server en https://www.npmjs.com/package/http-server